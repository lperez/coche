from Coche import Coche
import unittest

class CocheTestCase(unittest.TestCase):
    def test_aceleracion(self):
        coche01 = Coche("blanco", "fiat", "500", "9876zyx", 30)
        aceleracion = coche01.aceleracion()
        self.assertEqual(aceleracion, 60)

    def test_frenar(self):
        coche02 = Coche("blanco", "fiat", "500", "9876zyx", 30)
        frenar = coche02.frenar()
        self.assertEqual(frenar, 15)

if __name__ == '__main__':
    unittest.main()